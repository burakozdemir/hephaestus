[![Build status](https://gitlab.com/burakozdemir/hephaestus/badges/master/pipeline.svg)](https://gitlab.com/burakozdemir/hephaestus//commits/master)


## Requirements

The application runs in the docker environment. Therefore, you need Docker and docker-compose.

Tested successfully with:

- Docker (Docker version 19.03.6, build 369ce74a3c)
- Docker Compose (docker-compose version 1.28.0, build d02a7b1a)

## Setup

You need to run the following commands within the project directory to set up the project.

- `make setup`
- `make docker`

After the executions are complete, you can access the project by visiting http://127.0.0.1:3000

## Software stack

The project runs on the following software:

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Styled components](https://styled-components.com/)
- [Redux-Saga](https://redux-saga.js.org/)
- [json-server](https://github.com/typicode/json-server) (For mock API)

## CI/CD

Tests are run in GitLab CI environment everytime a commit is made to the master branch.

Pipelines can be seen here: https://gitlab.com/burakozdemir/hephaestus/-/pipelines

You can also run the tests manually by running `make test` within the project directory.

## Future improvements

- Add pagination or endless scroll for product list page
- Add responsiveness
- Consider implementing server side rendering for better SEO
- Automatic deployments to AWS
