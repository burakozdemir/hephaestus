import React from 'react';
import { render } from '@testing-library/react';

import { A } from '../index';
import { themes } from 'styles/theme/themes';
import { DefaultTheme, ThemeProvider } from 'styled-components';

const renderWithTheme = (theme?: DefaultTheme) => {
  return render(
    <ThemeProvider theme={themes.light}>
      <A />
    </ThemeProvider>,
  );
};

describe('<A />', () => {
  it('should render an <a> tag', () => {
    const a = renderWithTheme();
    expect(a.container.querySelector('a')).toBeInTheDocument();
  });

  it('should have theme', () => {
    const a = renderWithTheme();
    expect(a.container.firstChild).toHaveStyle(
      `background-color: ${themes.light.primary}`,
    );
  });
});
