import styled from 'styled-components/macro';

export const A = styled.a`
  display: inline-block;
  padding: 8px 15px;
  color: rgba(255, 255, 255, 1);
  font-size: 14px;
  line-height: 16px;
  font-weight: 500;
  background-color: ${p => p.theme.primary};
  text-decoration: none;
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05);
  border-radius: 2px;

  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;
