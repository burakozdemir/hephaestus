import React, { memo } from 'react';
import styled from 'styled-components/macro';

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

interface Props extends InputProps {
  id: string;
  name: string;
  value: string;
  label: string;
  className?: string;
  checked?: boolean;
}

export const Checkbox = memo(
  ({ label, id, name, value, className, ...restOf }: Props) => {
    return (
      <Wrapper className={className}>
        <input type="checkbox" id={id} name={name} value={value} {...restOf} />
        <label htmlFor={id}>{label}</label>
      </Wrapper>
    );
  },
);

const Wrapper = styled.div`
  input[type='checkbox'] {
    margin: 0;
    opacity: 0;
    width: 0;
    height: 0;
    padding: 0;

    + label {
      margin: 0;
      display: inline-block;
      padding-left: 26px;
      font-size: 13px;
      font-weight: 500;
      line-height: 18px;
      position: relative;
      cursor: pointer;
      color: ${p => p.theme.text};
      z-index: 1;
      white-space: nowrap;

      &::before {
        position: absolute;
        top: 1px;
        left: 0;
        display: inline-block;
        width: 16px;
        height: 16px;
        content: '';
        background-color: rgba(255, 255, 255, 1);
        border: 1px solid ${p => p.theme.borderLight};
        transition: all 0.1s;
      }

      &::after {
        display: none;
        content: '';
        position: absolute;
        width: 8px;
        height: 8px;
        top: 5px;
        left: 4px;
        background-color: ${p => p.theme.primary};
      }

      &:hover {
        &::before {
          border-color: ${p => p.theme.primary};
        }
      }
    }

    &:disabled {
      + label {
        opacity: 0.6;
        cursor: auto;

        &:hover {
          &::before {
            border-color: ${p => p.theme.borderLight};
          }
        }
      }
    }

    &:focus {
      + label {
        &::before {
          box-shadow: 0 0 0 3px
            ${p =>
              p.theme.primary.replace(
                /rgba?(\(\s*\d+\s*,\s*\d+\s*,\s*\d+)(?:\s*,.+?)?\)/,
                'rgba$1,0.2)',
              )};
        }
      }
    }

    &:checked {
      + label {
        color: ${p => p.theme.textSecondary};

        &::before {
          border-color: ${p => p.theme.primary};
        }

        &::after {
          display: inline-block;
        }
      }
    }
  }
`;
