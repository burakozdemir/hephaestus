import React from 'react';
import styled from 'styled-components/macro';

export function Logo() {
  return (
    <>
      <Title>Create Demand</Title>
      <Description>
        Search the product you need here. Use tags to find any alternative.
      </Description>
    </>
  );
}

const Title = styled.div`
  font-size: 18px;
  font-weight: 400;
  line-height: 21px;
  color: ${p => p.theme.textSecondary};
  margin-bottom: 6px;
`;

const Description = styled.div`
  font-size: 14px;
  font-weight: 400;
  line-height: 24px;
  color: ${p => p.theme.text};
`;
