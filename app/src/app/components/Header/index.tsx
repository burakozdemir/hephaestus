import React from 'react';
import styled from 'styled-components/macro';
import { Logo } from './Logo';

export function Header() {
  return (
    <Wrapper>
      <Logo />
    </Wrapper>
  );
}

const Wrapper = styled.header`
  flex: 0 0 100%;
  margin-bottom: 36px;
`;
