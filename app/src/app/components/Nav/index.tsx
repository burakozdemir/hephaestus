import React from 'react';
import styled from 'styled-components/macro';

import { Navbar } from 'app/components/Navbar';

export const Nav = () => {
  return (
    <Wrapper>
      <Navbar to={process.env.PUBLIC_URL + '/products'} className="active">
        1 Product
      </Navbar>
      <Navbar to="#" className="disabled">
        2 Addresses
      </Navbar>
      <Navbar to="#" className="disabled">
        3 Overview
      </Navbar>
    </Wrapper>
  );
};

const Wrapper = styled.main`
  display: flex;
  flex: 0 0 100%;
  margin-bottom: 26px;
`;
