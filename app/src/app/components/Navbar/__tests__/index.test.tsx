import React from 'react';
import { render } from '@testing-library/react';

import { Navbar } from '../index';
import { themes } from 'styles/theme/themes';
import { DefaultTheme } from 'styled-components';
import { MemoryRouter } from 'react-router-dom';

const renderWithTheme = (theme?: DefaultTheme) => {
  return render(
    <MemoryRouter>
      <Navbar to="/test" theme={theme || themes.light}>
        HeaderLink
      </Navbar>
    </MemoryRouter>,
  );
};

describe('<Navbar />', () => {
  it('should match snapshot', () => {
    const link = renderWithTheme();
    expect(link.container.firstChild).toMatchSnapshot();
  });

  it('should have theme', () => {
    const link = renderWithTheme();
    expect(link.container.firstChild).toHaveStyle(
      `color: ${themes.light.text}`,
    );
  });

  it('should have a class attribute', () => {
    const link = renderWithTheme();
    expect(link.queryByText('HeaderLink')).toHaveAttribute('class');
  });
});
