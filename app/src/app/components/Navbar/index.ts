import styled from 'styled-components/macro';
import { Link as RouterLink } from 'react-router-dom';

export const Navbar = styled(RouterLink)`
  min-width: 47px;
  padding: 17px 25px 13px 25px;
  color: ${p => p.theme.text};
  text-decoration: none;
  text-align: center;
  font-size: 14px;
  font-weight: 500;
  line-height: 16px;
  border-bottom: 2px solid ${p => p.theme.text};
  transition: all 0.1s;
  user-select: none;

  &:hover:not(.disabled),
  &.active {
    color: ${p => p.theme.primary};
    border-color: ${p => p.theme.primary};
  }

  &.disabled {
    opacity: 0.15;
    cursor: not-allowed;
    pointer-events: all;
  }
`;
