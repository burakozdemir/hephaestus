import styled from 'styled-components/macro';

export const P = styled.p`
  font-size: 14px;
  font-weight: normal;
  line-height: 24px;
  color: ${p => p.theme.textSecondary};
`;
