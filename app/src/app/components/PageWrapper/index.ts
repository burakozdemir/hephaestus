import styled from 'styled-components/macro';

export const PageWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  width: 1488px;
  padding: 65px;
  margin: 0 auto;
  box-sizing: content-box;
`;
