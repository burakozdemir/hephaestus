import React, { useState, useCallback, useEffect } from 'react';
import styled from 'styled-components/macro';

import { ReactComponent as ScrollTopIcon } from './assets/Subtract.svg';

export function ScrollTopButton() {
  const [showScroll, setShowScroll] = useState(false);

  const scrollTop = useCallback(() => {
    try {
      // trying to use new API - https://developer.mozilla.org/en-US/docs/Web/API/Window/scrollTo
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    } catch (error) {
      // just a fallback for older browsers
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }, []);

  useEffect(() => {
    const checkScrollTop = () => {
      if (!showScroll && window.pageYOffset > 50) {
        setShowScroll(true);
      } else if (showScroll && window.pageYOffset <= 50) {
        setShowScroll(false);
      }
    };

    window.addEventListener('scroll', checkScrollTop);

    return () => {
      window.removeEventListener('scroll', checkScrollTop);
    };
  }, [showScroll]);

  return (
    <Wrapper onClick={scrollTop} className={showScroll ? '' : 'hidden'}>
      <ScrollTopIcon width="65px" height="65px" />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  position: sticky;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  bottom: 20px;
  cursor: pointer;
  z-index: 1000;
  animation: fadeIn 0.3s;
  transition: opacity 0.4s;
  opacity: 1;

  &.hidden {
    display: none;
  }

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }

    100% {
      opacity: 1;
    }
  }
`;
