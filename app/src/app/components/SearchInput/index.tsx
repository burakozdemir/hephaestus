import React, { memo } from 'react';
import styled from 'styled-components/macro';

import { ReactComponent as SearchIcon } from './assets/search.svg';

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

interface Props extends InputProps {
  id: string;
  value: string;
  className?: string;
}

export const SearchInput = memo(
  ({ id, className, value, ...restOf }: Props) => {
    return (
      <Wrapper className={className}>
        <SearchIcon className="search-icon" width="24px" height="24px" />
        <input type="text" id={id} value={value} {...restOf} />
      </Wrapper>
    );
  },
);

const Wrapper = styled.div`
  position: relative;

  input[type='text'] {
    border: 1px solid ${p => p.theme.borderLight};
    border-radius: 2px;
    background-color: ${p => p.theme.backgroundVariant};
    color: ${p => p.theme.textSecondary};
    display: block;
    width: 100%;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    outline: none;
    height: 36px;
    padding: 8px 7px 8px 40px;

    &::placeholder {
      color: ${p => p.theme.text};
    }

    &:focus {
      border-color: ${p => p.theme.primary};
      box-shadow: 0 0 0 3px
        ${p =>
          p.theme.primary.replace(
            /rgba?(\(\s*\d+\s*,\s*\d+\s*,\s*\d+)(?:\s*,.+?)?\)/,
            'rgba$1,0.2)',
          )};
    }
  }

  .search-icon {
    position: absolute;
    top: calc(50% - 12px);
    left: 7px;
  }
`;
