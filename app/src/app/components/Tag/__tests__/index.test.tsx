import React from 'react';
import { render } from '@testing-library/react';

import { Tag } from '../index';
import { themes } from 'styles/theme/themes';
import { DefaultTheme, ThemeProvider } from 'styled-components';

const renderWithTheme = (theme?: DefaultTheme) => {
  return render(
    <ThemeProvider theme={themes.light}>
      <Tag>Name</Tag>
    </ThemeProvider>,
  );
};

describe('<Tag />', () => {
  it('should match snapshot', () => {
    const tag = renderWithTheme();
    expect(tag.container.firstChild).toMatchSnapshot();
  });

  it('should have theme', () => {
    const tag = renderWithTheme();
    expect(tag.container.firstChild).toHaveStyle(
      `color: ${themes.light.primary}`,
    );
    expect(tag.container.firstChild).toHaveStyle(
      `background-color: rgba(18,184,255,0.15)`,
    );
  });
});
