import styled from 'styled-components/macro';

export const Tag = styled.div`
  font-size: 12px;
  font-weight: 500;
  line-height: 14px;
  padding: 5px 10px;
  background-color: ${p =>
    p.theme.primary.replace(
      /rgba?(\(\s*\d+\s*,\s*\d+\s*,\s*\d+)(?:\s*,.+?)?\)/,
      'rgba$1,0.15)',
    )};
  color: ${p => p.theme.primary};
  border-radius: 4px;
  white-space: nowrap;
`;
