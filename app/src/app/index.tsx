import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';

import { GlobalStyle } from '../styles/global-styles';

import { ProductsPage } from './pages/ProductsPage/Loadable';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';

export function App() {
  return (
    <BrowserRouter>
      <Helmet titleTemplate="%s - Hephaestus" defaultTitle="Hephaestus">
        <meta name="description" content="Hephaestus" />
      </Helmet>

      <Switch>
        <Route
          exact
          path={process.env.PUBLIC_URL + '/products'}
          component={ProductsPage}
        />
        <Route exact path={process.env.PUBLIC_URL + '/'}>
          <Redirect to={process.env.PUBLIC_URL + '/products'} />
        </Route>
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </BrowserRouter>
  );
}
