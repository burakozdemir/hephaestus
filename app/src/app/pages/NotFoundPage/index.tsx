import React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';

import { P } from 'app/components/P';
import { Navbar } from 'app/components/Navbar';

export function NotFoundPage() {
  return (
    <>
      <Helmet>
        <title>404 Page Not Found</title>
        <meta name="description" content="Page not found" />
      </Helmet>
      <Wrapper>
        <P>Page not found.</P>
        <Navbar to={process.env.PUBLIC_URL + '/products'}>
          Return to Products Page
        </Navbar>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
