import React, { useState } from 'react';
import styled from 'styled-components/macro';

import { Product } from 'types/Product';
import { A } from 'app/components/A';
import { P } from 'app/components/P';
import { Tag } from 'app/components/Tag';
import { Radio } from 'app/components/Radio';

interface Props {
  product: Product;
}

export function ProductDetails({ product }: Props) {
  const [selectedOption, setSelectedOption] = useState('');
  const handleOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setSelectedOption(value);
  };

  return (
    <Wrapper>
      <Title>Product Details</Title>
      <Content>
        <Name>{product.name}</Name>
        <Tags>
          {product.tags.length > 0
            ? product.tags.map((tag, index) => (
                <Tag key={`${tag}-${index}`}>{tag}</Tag>
              ))
            : null}
        </Tags>
        <ManufacturerLinkWrapper>
          <A href={product.manufacturer_url} target="_blank">
            Go to Manufacturer
          </A>
        </ManufacturerLinkWrapper>
        <P>{product.description}</P>
        <Options>
          {product.options.length > 0
            ? product.options.map((option, index) => (
                <div key={`${option}-${index}`}>
                  <Radio
                    id={`product_options-${index}`}
                    name="product_options"
                    label={`Option ${index + 1}`}
                    value={option}
                    onChange={handleOptionChange}
                    checked={option === selectedOption}
                  />
                  <P>{option}</P>
                </div>
              ))
            : null}
        </Options>
      </Content>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  align-self: flex-start;
  position: sticky;
  top: 0;
  width: 320px;
  margin-top: 1.25px;
  background: rgba(255, 255, 255, 1);
  box-shadow: 0px 1px 2px hsla(0, 0%, 0%, 0.08);
  border-radius: 4px;
`;

const Title = styled.div`
  font-size: 16px;
  line-height: 18.75px;
  font-weight: 500;
  color: ${p => p.theme.textSecondary};
  border-bottom: 1px solid ${p => p.theme.border};
  padding: 19px 24px 16px;
`;

const Content = styled.div`
  padding: 42px 24px 20px;
`;

const Name = styled.div`
  font-size: 14px;
  line-height: 20px;
  font-weight: 500;
  color: ${p => p.theme.textSecondary};
  margin-bottom: 20px;
`;

const Tags = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 24px;

  div {
    margin-right: 8px;
    margin-bottom: 8px;
  }
`;

const ManufacturerLinkWrapper = styled.div`
  margin-bottom: 10px;
`;

const Options = styled.div`
  margin-top: 8px;
`;
