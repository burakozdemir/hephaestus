import React from 'react';
import styled from 'styled-components/macro';

import { Tag } from 'app/components/Tag';

interface Props {
  name: string;
  tags: Array<string>;
  category: string;
  onClick: () => void;
}

export function Product({ name, tags, category, onClick }: Props) {
  return (
    <Wrapper onClick={onClick}>
      <Title>{name}</Title>
      <Tags>
        {tags.length > 0
          ? tags.map((tag, index) => <Tag key={`${tag}-${index}`}>{tag}</Tag>)
          : null}
      </Tags>
      <Category>{category}</Category>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding: 14px 24px;
  margin-bottom: 8px;
  background: rgba(255, 255, 255, 1);
  box-shadow: 0px 1px 2px hsla(0, 0%, 0%, 0.08);
  border-radius: 4px;
  cursor: pointer;
`;

const Title = styled.div`
  flex: 0 0 100%;
  margin-bottom: 14px;
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
  color: ${p => p.theme.textSecondary};
`;

const Tags = styled.div`
  flex: 0 0 80%;
  display: flex;
  flex-wrap: wrap;

  div {
    margin-right: 8px;
    margin-bottom: 8px;
  }
`;

const Category = styled.div`
  position: absolute;
  right: 24px;
  flex: 0 0 20%;
  text-align: right;
  font-size: 14px;
  line-height: 16px;
  color: ${p => p.theme.text};
`;
