import React from 'react';
import { Store } from '@reduxjs/toolkit';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { HelmetProvider } from 'react-helmet-async';
import { ThemeProvider } from 'styled-components';

import { ProductList, productListErrorText } from '..';
import { configureAppStore } from 'store/configureStore';
import { productListActions as actions, initialState } from '../slice';
import { ProductListErrorType } from '../slice/types';
import { themes } from 'styles/theme/themes';

function* mockProductListSaga() {}

jest.mock('../slice/saga', () => ({
  productListSaga: mockProductListSaga,
}));

const renderProductList = (store: Store) =>
  render(
    <Provider store={store}>
      <ThemeProvider theme={themes.light}>
        <HelmetProvider>
          <ProductList />
        </HelmetProvider>
      </ThemeProvider>
    </Provider>,
  );

describe('<ProductList />', () => {
  let store: ReturnType<typeof configureAppStore>;
  let component: ReturnType<typeof renderProductList>;

  beforeEach(() => {
    store = configureAppStore();
    component = renderProductList(store);
    store.dispatch(actions.productsLoaded([]));
    expect(store.getState().productList).toEqual(initialState);
  });
  afterEach(() => {
    component.unmount();
  });

  it("shouldn't fetch products on mount if product name filter is not applied", () => {
    store.dispatch(actions.changeProductName(''));
    store.dispatch(actions.productsLoaded([]));
    component.unmount();
    component = renderProductList(store);
    expect(store.getState().productList.loading).toBe(false);
  });

  it('should dispatch action on product name change', () => {
    const input = component.container.querySelector('input[type="text"]');
    fireEvent.change(input!, { target: { value: 'test product' } });
    expect(store.getState().productList.loading).toBe(true);
  });

  it('should change product name field value on action', () => {
    const value = 'test';
    const form = renderProductList(store);

    const input = form.container.querySelector('input');
    fireEvent.change(input!, { target: { value: value } });

    expect(form.container.querySelector('input')?.value).toBe(value);
  });

  it('should display loading indicator when state is loading', () => {
    store.dispatch(actions.loadProducts());
    expect(component.container.querySelector('circle')).toBeInTheDocument();
  });

  it('should display list when products not empty', () => {
    const productName = 'test product';
    store.dispatch(
      actions.productsLoaded([
        {
          id: 'test',
          name: productName,
          description: 'test',
          manufacturer_url: 'url',
          category: 'test',
          tags: [],
          options: [],
        } as any,
      ]),
    );
    expect(component.queryByText(productName)).toBeInTheDocument();
  });

  it('should display error when productListError fired', () => {
    let error = ProductListErrorType.NOT_FOUND;
    store.dispatch(actions.productListError(error));
    expect(
      component.queryByText(productListErrorText(error)),
    ).toBeInTheDocument();

    error = ProductListErrorType.NOT_FILTERED;
    store.dispatch(actions.productListError(error));
    expect(
      component.queryByText(productListErrorText(error)),
    ).toBeInTheDocument();

    error = ProductListErrorType.RESPONSE_ERROR;
    store.dispatch(actions.productListError(error));
    expect(
      component.queryByText(productListErrorText(error)),
    ).toBeInTheDocument();
  });
});
