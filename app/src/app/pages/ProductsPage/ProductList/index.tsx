import React, { useEffect } from 'react';
import styled from 'styled-components/macro';
import { useSelector, useDispatch } from 'react-redux';

import {
  selectProductName,
  selectSelectedProduct,
  selectProducts,
  selectLoading,
  selectError,
  selectProductCategories,
} from './slice/selectors';
import { ProductListErrorType } from './slice/types';
import { useProductListSlice } from './slice';
import { Checkbox } from 'app/components/Checkbox';
import { SearchInput } from 'app/components/SearchInput';
import { LoadingIndicator } from 'app/components/LoadingIndicator';
import { ScrollTopButton } from 'app/components/ScrollTopButton';
import { ProductDetails } from '../ProductDetails';
import { Product } from './Product';

interface CheckboxProps {
  name: string;
  label: string;
  value: string;
}
const productCategoryCheckboxes: Array<CheckboxProps> = [
  {
    name: 'software_development',
    label: 'Software Development',
    value: 'Software Development',
  },
  {
    name: 'daily_business',
    label: 'Daily Business',
    value: 'Daily Business',
  },
  {
    name: 'graphic_editors',
    label: 'Graphic Editors',
    value: 'Graphic Editors',
  },
  {
    name: 'text_editors',
    label: 'Text Editors',
    value: 'Text Editors',
  },
  {
    name: 'management_tools',
    label: 'Management Tools',
    value: 'Management Tools',
  },
];

export function ProductList() {
  const { actions } = useProductListSlice();

  const productName = useSelector(selectProductName);
  const productCategories = useSelector(selectProductCategories);
  const products = useSelector(selectProducts);
  const selectedProduct = useSelector(selectSelectedProduct);
  const isLoading = useSelector(selectLoading);
  const error = useSelector(selectError);

  const dispatch = useDispatch();

  const onChangeProductName = (evt: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(actions.changeProductName(evt.currentTarget.value));
    dispatch(actions.loadProducts());
  };

  const onChangeProductCategory = (
    evt: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const categories = [...productCategories];
    const changedCategory = evt.currentTarget.value;

    if (evt.currentTarget.checked) {
      categories.push(changedCategory);
    } else {
      const index = categories.findIndex(c => c === changedCategory);
      categories.splice(index, 1);
    }

    dispatch(actions.changeProductCategory(categories));
    dispatch(actions.loadProducts());
  };

  const onSelectProduct = (product: any) => {
    dispatch(actions.selectProduct(product));
  };

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    if (
      (productName && productName.trim().length > 0) ||
      (productCategories && productCategories.length > 0)
    ) {
      dispatch(actions.loadProducts());
    }
  });

  return (
    <>
      <ProductListWrapper>
        <FilterWrapper>
          <Title>I’m looking for...</Title>
          <Filters>
            <ProductCategoryWrapper>
              {productCategoryCheckboxes.map((category, index) => (
                <Checkbox
                  key={category.name}
                  id={`${category.name}-${index}`}
                  name={category.name}
                  label={category.label}
                  value={category.value}
                  className="checkbox"
                  onChange={onChangeProductCategory}
                  checked={
                    productCategories &&
                    productCategories.includes(category.value)
                  }
                />
              ))}
            </ProductCategoryWrapper>
            <SearchInput
              id="product_search"
              className="search-input"
              placeholder="Type here..."
              value={productName}
              onChange={onChangeProductName}
            />
          </Filters>
        </FilterWrapper>
        {isLoading ? (
          <LoadingIndicatorWrapper>
            <LoadingIndicator />
          </LoadingIndicatorWrapper>
        ) : products?.length > 0 ? (
          <div>
            {products.map(product => (
              <Product
                key={product.id}
                name={product.name}
                tags={product.tags}
                category={product.category}
                onClick={() => onSelectProduct(product)}
              />
            ))}
          </div>
        ) : error ? (
          <ErrorText>{productListErrorText(error)}</ErrorText>
        ) : null}
        <ScrollTopButton />
      </ProductListWrapper>
      {selectedProduct && <ProductDetails product={selectedProduct} />}
    </>
  );
}

export const productListErrorText = (error: ProductListErrorType) => {
  switch (error) {
    case ProductListErrorType.NOT_FOUND:
      return 'No products found.';
    case ProductListErrorType.NOT_FILTERED:
      return 'Please apply a filter to view available products.';
    default:
      return 'An error has occurred!';
  }
};

const ProductListWrapper = styled.div`
  width: 1152px;
`;

const Title = styled.div`
  font-size: 16px;
  line-height: 20px;
  font-weight: 500;
  color: ${p => p.theme.textSecondary};
  border-bottom: 1px solid ${p => p.theme.border};
  padding: 13px 24px 22px;
`;

const FilterWrapper = styled.div`
  background: rgba(255, 255, 255, 1);
  box-shadow: 0px 1px 2px hsla(0, 0%, 0%, 0.08);
  border-radius: 4px;
  margin-bottom: 16px;
`;

const Filters = styled.div`
  padding: 32px 24px;
`;

const ProductCategoryWrapper = styled.div`
  display: flex;
  margin-bottom: 20px;

  .checkbox {
    margin-right: 80px;
  }
`;

const ErrorText = styled.div`
  color: ${p => p.theme.text};
  text-align: center;
`;

const LoadingIndicatorWrapper = styled.div`
  display: flex;
  justify-content: center;
`;
