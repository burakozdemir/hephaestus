import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { Product } from 'types/Product';
import { productListSaga } from './saga';
import { ProductListState, ProductListErrorType } from './types';

export const initialState: ProductListState = {
  productName: '',
  productCategories: [],
  products: [],
  selectedProduct: null,
  loading: false,
  error: null,
};

const slice = createSlice({
  name: 'productList',
  initialState,
  reducers: {
    changeProductName(state, action: PayloadAction<string>) {
      state.productName = action.payload;
    },
    changeProductCategory(state, action: PayloadAction<Array<string>>) {
      state.productCategories = action.payload;
    },
    selectProduct(state, action: PayloadAction<Product>) {
      state.selectedProduct = action.payload;
    },
    loadProducts(state) {
      state.loading = true;
      state.error = null;
      state.products = [];
      state.selectedProduct = null;
    },
    productsLoaded(state, action: PayloadAction<Product[]>) {
      const products = action.payload;
      state.products = products;
      state.loading = false;
    },
    productListError(state, action: PayloadAction<ProductListErrorType>) {
      state.error = action.payload;
      state.loading = false;
      state.products = [];
    },
  },
});

export const { actions: productListActions, reducer } = slice;

export const useProductListSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: productListSaga });
  return { actions: slice.actions };
};
