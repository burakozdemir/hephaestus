import { call, put, select, takeLatest, delay } from 'redux-saga/effects';
import { request } from 'utils/request';
import { selectProductName, selectProductCategories } from './selectors';
import { productListActions as actions } from '.';
import { Product } from 'types/Product';
import { ProductListErrorType } from './types';

/**
 * Product list request/response handler
 */
export function* getProducts() {
  yield delay(500);
  // Select product name and categories from store
  const productName: string = yield select(selectProductName);
  const productCategories: Array<string> = yield select(
    selectProductCategories,
  );

  if (productName.length === 0 && productCategories.length === 0) {
    yield put(actions.productListError(ProductListErrorType.NOT_FILTERED));
    return;
  }

  let queryParams = new URLSearchParams();
  if (productName.length > 0) {
    queryParams.append('name_like', productName);
  }

  productCategories.forEach(value => {
    queryParams.append('category', value);
  });

  const requestURL = `http://localhost:80/products/?${queryParams.toString()}`;

  try {
    const products: Product[] = yield call(request, requestURL);
    if (products?.length > 0) {
      yield put(actions.productsLoaded(products));
    } else {
      yield put(actions.productListError(ProductListErrorType.NOT_FOUND));
    }
  } catch (err) {
    if (err.response?.status === 404) {
      yield put(actions.productListError(ProductListErrorType.NOT_FOUND));
    } else {
      yield put(actions.productListError(ProductListErrorType.RESPONSE_ERROR));
    }
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* productListSaga() {
  yield takeLatest(actions.loadProducts.type, getProducts);
}
