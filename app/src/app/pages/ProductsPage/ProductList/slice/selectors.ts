import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

// First select the relevant part from the state
const selectDomain = (state: RootState) => state.productList || initialState;

export const selectProductName = createSelector(
  [selectDomain],
  productListState => productListState.productName,
);

export const selectProductCategories = createSelector(
  [selectDomain],
  productListState => productListState.productCategories,
);

export const selectLoading = createSelector(
  [selectDomain],
  productListState => productListState.loading,
);

export const selectError = createSelector(
  [selectDomain],
  productListState => productListState.error,
);

export const selectSelectedProduct = createSelector(
  [selectDomain],
  productListState => productListState.selectedProduct,
);

export const selectProducts = createSelector(
  [selectDomain],
  productListState => productListState.products,
);
