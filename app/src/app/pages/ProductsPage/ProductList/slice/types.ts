import { Product } from 'types/Product';

/* --- STATE --- */
export interface ProductListState {
  productName: string;
  productCategories: Array<string>;
  loading: boolean;
  error?: ProductListErrorType | null;
  products: Product[];
  selectedProduct: Product | null;
}

export enum ProductListErrorType {
  RESPONSE_ERROR = 1,
  NOT_FOUND = 2,
  NOT_FILTERED = 3,
}

export type ContainerState = ProductListState;
