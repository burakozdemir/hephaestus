import React from 'react';
import { Helmet } from 'react-helmet-async';

import { Header } from 'app/components/Header';
import { PageWrapper } from 'app/components/PageWrapper';
import { Nav } from 'app/components/Nav';
import { ProductList } from './ProductList';

export function ProductsPage() {
  return (
    <>
      <Helmet>
        <title>Projects</title>
        <meta name="description" content="Projects" />
      </Helmet>
      <PageWrapper>
        <Header />
        <Nav />
        <ProductList />
      </PageWrapper>
    </>
  );
}
