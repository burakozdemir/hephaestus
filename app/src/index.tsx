/**
 * This is the entry file for the application, only setup and boilerplate code.
 */

import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import FontFaceObserver from 'fontfaceobserver';
import { ThemeProvider } from 'styled-components';

// Use consistent styling
import 'sanitize.css/sanitize.css';

import { App } from 'app';

import { HelmetProvider } from 'react-helmet-async';

import { configureAppStore } from 'store/configureStore';

import reportWebVitals from 'reportWebVitals';

import { themes } from 'styles/theme/themes';

// Observe Roboto font loading.
const robotoFontObserver = new FontFaceObserver('Roboto', {});

robotoFontObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
});

const store = configureAppStore();
const MOUNT_NODE = document.getElementById('root') as HTMLElement;

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={themes.light}>
      <HelmetProvider>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </HelmetProvider>
    </ThemeProvider>
  </Provider>,
  MOUNT_NODE,
);

/* The application performance can be measured by WebVitals. We need to provide `console.log` reference to
 * `reportWebVitals` function in order to do that. More information can be found here: https://bit.ly/CRA-vitals
 * */
reportWebVitals();
