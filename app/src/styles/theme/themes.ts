const lightTheme = {
  primary: 'rgba(18,184,255,1)',
  text: 'rgba(132,146,166,1)',
  textSecondary: 'rgba(60,72,88,1)',
  background: 'rgba(245,246,250,1)',
  backgroundVariant: 'rgba(245,246,247,1)',
  border: 'rgba(235,235,235,1)',
  borderLight: 'rgba(0,0,0,0.05)',
};

export type Theme = typeof lightTheme;

export const themes = {
  light: lightTheme,
};
