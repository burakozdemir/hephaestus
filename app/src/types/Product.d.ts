export interface Product {
  id: number;
  name: string;
  category: string;
  manufacturer_url: string;
  description: string;
  tags: Array<string>;
  options: Array<string>;
}
