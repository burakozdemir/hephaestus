import { ProductListState } from 'app/pages/ProductsPage/ProductList/slice/types';

export interface RootState {
  productList?: ProductListState;
}
